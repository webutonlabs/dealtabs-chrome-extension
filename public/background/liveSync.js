/* global chrome */

const API_HOST = 'https://api.dealtabs.com';
const APP_DOMAIN = 'dealtabs.com';

const getAppCookies = (successCallback) => {
    const findCookieOrNull = (cookieName, cookies) => {
        let cookie = cookies.find(c => c.name === cookieName);

        if (cookie === undefined || cookie.value === null || cookie.value === '') {
            return null;
        }

        return cookie;
    }

    chrome.cookies.getAll({domain: APP_DOMAIN}, (cookies) => {
        if (cookies !== undefined) {
            /**
             * This feature is available only for premium users.
             * To prevent sending pointless requests from non-premium users
             * or from devices with disabled live-sync we check this cookie.
             *
             * This cookie is set from the extension in LiveSynchronizationComponent
             */
            const liveSyncEnabled = findCookieOrNull('live-sync', cookies);
            const tokenCookie = findCookieOrNull('token', cookies);
            const deviceIdCookie = findCookieOrNull('device-id', cookies);

            if (null !== tokenCookie && null !== deviceIdCookie && null !== liveSyncEnabled) {
                const token = tokenCookie.value;
                const deviceId = deviceIdCookie.value;

                if (liveSyncEnabled.value !== 'false') {
                    successCallback(token, deviceId);
                }
            }
        }
    });
}
const apiXhrFactory = (method, uri, token, deviceId) => {
    const xhr = new XMLHttpRequest();
    xhr.open(method, `${API_HOST}${uri}`, true);
    xhr.responseType = 'json';
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("X-AUTH-TOKEN", token);
    xhr.setRequestHeader("X-DEVICE-ID", deviceId);

    return xhr;
}

const sendApiRequestToUpdateTabs = () => {
    getAppCookies((token, deviceId) => {
        chrome.tabs.query({}, (tabs) => {
            let data = {tabs: []};

            for (const tab of tabs) {
                let tabIcon = tab.favIconUrl === undefined ? null : tab.favIconUrl;
                if (null === tabIcon || tabIcon.startsWith('data')) {
                    tabIcon = `https://${new URL(tab.url).hostname}/favicon.ico`;
                }

                data.tabs.push({
                    name: tab.title,
                    icon: tabIcon,
                    url: tab.url,
                    is_active: tab.active
                })
            }

            const xhr = apiXhrFactory('PUT','/live-sync/replace-synced-tabs', token, deviceId);
            xhr.send(JSON.stringify(data));
        })
    });
}

chrome.tabs.onUpdated.addListener(() => {
    sendApiRequestToUpdateTabs();
});

chrome.tabs.onCreated.addListener(() => {
    sendApiRequestToUpdateTabs();
});

chrome.tabs.onAttached.addListener(() => {
    sendApiRequestToUpdateTabs();
});

chrome.tabs.onReplaced.addListener(() => {
    sendApiRequestToUpdateTabs();
});

chrome.tabs.onActivated.addListener(() => {
    sendApiRequestToUpdateTabs();
});

chrome.tabs.onMoved.addListener(() => {
    sendApiRequestToUpdateTabs();
});