function sliceString(string, length) {
    if (length === undefined) {
        length = 55;
    }

    if (string.length > length) {
        string = string.slice(0, length);

        return `${string}...`;
    }

    return string;
}

function filterByEnableStatus(entities) {
    let enabled = [];
    let disabled = [];

    entities.forEach(item => {
        item['is_enabled'] ?  enabled.push(item) : disabled.push(item);
    })

    return [enabled, disabled];
}

const isUserPremium = (user) => {
    return null === user.membership.subscriptionExpires
        ? false
        : (new Date(user.membership.subscriptionExpires).getTime() > new Date().getTime());
}

function makeArrayOfIds(entities) {
    let arr = [];
    entities.forEach(item => {
        arr.push(Number(item.id));
    })

    return arr;
}

function windowIncludesTabByUrl(window, checkUrl) {
    let tabs = [];
    for (const tab of window.tabs) {
        tabs.push(tab.url);
    }

    return tabs.includes(checkUrl);
}

function uniqueTabsToSave(window, tabsFromWindow) {
    let tabs = [], uniqueTabs = [];

    for (const tab of window.tabs) {
        tabs.push(tab.url);
    }

    for (const newTab of tabsFromWindow) {
        if (!tabs.includes(newTab.url)) {
            tabs.push(newTab.url);
            uniqueTabs.push(newTab);
        }
    }

    return uniqueTabs;
}

export {
    sliceString,
    filterByEnableStatus,
    isUserPremium,
    makeArrayOfIds,
    windowIncludesTabByUrl,
    uniqueTabsToSave
}