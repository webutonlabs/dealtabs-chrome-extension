import React from "react";
import {AppContext} from "../../components/App/AppContext";

class LoadMore extends React.Component {
    static contextType = AppContext;

    render() {
        const t = this.context.t;

        return (
            <div className={'row justify-content-center mt-10'}>
                <div className="invoice-buttons text-center">
                    <button onClick={this.props.loadMore} className={'invoice-btn btn text-white-hover'}>
                        {t('Load more')}
                    </button>
                </div>
            </div>
        )
    }
}

export default LoadMore;