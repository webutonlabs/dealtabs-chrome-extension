const TYPE_BASIC = 'basic';
const TYPE_PREMIUM = 'premium';

export {
    TYPE_BASIC,
    TYPE_PREMIUM
}