class SessionFactory {
    constructor(name = null, windows = []) {
        this.windows = windows;
        this.name = null;
    }

    json = () => {
        const date = new Date();

        return {
            'name': this.name !== null ? this.name : `#${(date.toLocaleDateString(undefined, {
                weekday: 'long',
                year: 'numeric',
                month: 'long',
                day: 'numeric'
            }))} ${date.toLocaleTimeString()}`,
            'windows': this.windows
        };
    }
}

export default SessionFactory;