class TabFactory {
    constructor(tabFromBrowserApi) {
        this.tabData = tabFromBrowserApi;
    }

    json = () => {
        let tab = {};

        if (this.tabData.favIconUrl !== undefined && this.tabData.favIconUrl.startsWith('data')) { // base 64 not support
            this.tabData.favIconUrl = `https://${new URL(this.tabData.url).hostname}/favicon.ico`;
        }

        tab.name = this.tabData.title;
        tab.url = this.tabData.url;
        tab.icon = this.tabData.favIconUrl === undefined ? null : this.tabData.favIconUrl;

        return tab;
    }

    static createMany = (tabs) => {
        let normalizedTabs = [];

        tabs.forEach(tabData => {
            normalizedTabs.push(new TabFactory(tabData).json());
        });

        return normalizedTabs;
    }
}

export default TabFactory;