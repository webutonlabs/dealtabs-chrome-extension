import TabFactory from "./TabFactory";

class WindowFactory {
    constructor(tabs = []) {
        this.tabs = tabs;
    }

    json = () => {
        return {name: null, tabs: this.tabs};
    }

    createManyFromStructuredTabs = (structuredTabs) => {
        let windows = [];
        for (const [windowId, tabs] of Object.entries(structuredTabs)) {
            let normalizedTabs = [];

            tabs.forEach(tabData => {
                normalizedTabs.push(new TabFactory(tabData).json());
            });

            windows.push(new WindowFactory(normalizedTabs).json())
        }

        return windows;
    }
}

export default WindowFactory;