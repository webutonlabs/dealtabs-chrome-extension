/*global chrome*/

class BrowserCookiesApi {
    constructor() {
        this.cookies = [];
    }

    iterateAll = (searchDomain, callback) => {
        chrome.cookies.getAll({domain: searchDomain}, callback);
    }

    setCookies = (cookies) => {
        this.cookies = cookies;
    }

    findCookieOrNull = (cookieName) => {
        let cookie = this.cookies.find(c => c.name === cookieName);

        if (cookie === undefined || cookie.value === null || cookie.value === '') {
            return null;
        }

        return cookie;
    }

    createCookie = (
        successCallback,
        name,
        value,
        expirationDate,
    ) => {
        chrome.cookies.set({
            url: process.env.REACT_APP_SITE_HOST,
            name: name,
            value: value,
            expirationDate: expirationDate,
        }, successCallback)
    }
}

export default BrowserCookiesApi;