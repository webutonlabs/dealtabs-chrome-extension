/*global chrome*/

class BrowserWindowsApi {
    findFocused = (successCallback) => {
        chrome.windows.getAll({}, (windows) => {
            let focusedWindow = windows.find(item => item.focused === true);

            if (undefined === focusedWindow) {
                successCallback(windows[0]);
                return;
            }

            successCallback(focusedWindow);
        })
    }

    openWindows = (windows) => {
        windows.forEach((sessionWindow) => {
            let tabs = [];
            sessionWindow.tabs.forEach((tab) => {
                tabs.push(tab.url);
            })

            chrome.windows.create({
                'url': tabs,
                'type': 'normal'
            });
        })
    }
}

export default BrowserWindowsApi;