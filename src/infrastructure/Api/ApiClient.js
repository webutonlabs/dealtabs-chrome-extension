import axios from 'axios'

const apiClient = (token, deviceId) => {
    return axios.create({
        baseURL: `${process.env.REACT_APP_API_HOST}${process.env.REACT_APP_API_URI_PREFIX}`,
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
            'X-AUTH-TOKEN': token,
            'X-DEVICE-ID': deviceId
        }
    });
}

export default apiClient;