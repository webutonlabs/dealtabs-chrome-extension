/*global chrome*/

class BrowserTabsApi {
    createTab = (url, isActive = true) => {
        chrome.tabs.create({url: url, active: isActive});
    }

    findByWindow = (windowId, successCallback) => {
        chrome.tabs.query({windowId: windowId}, (tabs) => {
            successCallback(tabs);
        })
    }

    iterateAll = (successCallback) => {
        chrome.tabs.query({}, (tabs) => {
            successCallback(tabs);
        })
    }

    getStructuredByWindows = (successCallback) => {
        this.iterateAll((tabs) => {
            let structuredTabs = {};

            tabs.forEach(tab => {
                if (structuredTabs[tab.windowId] === undefined) {
                    structuredTabs[tab.windowId] = [];
                }

                structuredTabs[tab.windowId].push(tab);
            });

            successCallback(structuredTabs);
        })
    }
}

export default BrowserTabsApi;