import React from "react";

class DangerAlert extends React.Component {
    render() {
        return (
            <div
                style={{padding: '7px 16px', fontSize: '11px', borderRadius: 0}}
                className={'alert alert-danger mb-0 text-center'}
                dangerouslySetInnerHTML={{__html: this.props.content}}
            />
        )
    }
}

export default DangerAlert;