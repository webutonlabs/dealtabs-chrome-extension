import React from "react";

class TabIcon extends React.Component {
    onError = (e) => {
        e.target.src = `${process.env.REACT_APP_SITE_HOST}/no-tab-icon-replacer.png`;
    }

    render() {
        const noIconReplacer = `${process.env.REACT_APP_SITE_HOST}/no-tab-icon-replacer.png`;
        let src = this.props.src;

        if (src === '' || src === undefined || src === null) {
            src = noIconReplacer;
        }

        return (
            <img
                alt={'icon'}
                width="16px"
                height="16px"
                src={src}
                onError={this.onError}
            />
        )
    }
}

export default TabIcon;