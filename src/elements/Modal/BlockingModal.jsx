import React from "react";
import {AppContext} from "../../components/App/AppContext";
import {Modal} from "react-bootstrap";
import apiClient from "../../infrastructure/Api/ApiClient";
import BrowserTabsApi from "../../infrastructure/Api/BrowserTabsApi";

class BlockingModal extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
    }

    componentDidMount() {
        apiClient(this.context.token, this.context.deviceId).get('/parameters/blocking-version').then(res => {
            const currentVersion = parseFloat(process.env.REACT_APP_EXTENSION_VERSION);

            if (currentVersion < res.data.version) {
                this.setState({show: true});
            }
        });
    }

    openChrome = () => {
        new BrowserTabsApi().createTab(process.env.REACT_APP_GOOGLE_CHROME_STORE_URL);
    }

    openFirefox = () => {
        new BrowserTabsApi().createTab(process.env.REACT_APP_FIREFOX_STORE_URL);
    }

    render() {
        return (
            <Modal show={this.state.show} onHide={() => {}} centered={true} size={'lg'}>
                <Modal.Header style={{borderBottom: 'none'}}>
                    <Modal.Title className={'text-center'}>{this.context.t('new-version-available')}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div>{this.context.t('please-update')}</div>
                    <div className={'mt-3'}>
                        <button className={'btn btn-link'} onClick={this.openChrome}>Google Chrome</button>
                    </div>
                    <div className={'mt-1'}>
                        <button className={'btn btn-link'} onClick={this.openFirefox}>Mozilla Firefox</button>
                    </div>
                </Modal.Body>
            </Modal>
        )
    }
}

export default BlockingModal;