import React from "react";
import HeaderArea from "./HeaderArea";
import Notifications from "../components/Notification/Notifications";
import BlockingModal from "../elements/Modal/BlockingModal";

class Layout extends React.Component {
    render() {
        return (
            <>
                <Notifications />
                <div className="horizontal-main-wrapper">
                    <HeaderArea setRoute={this.props.setRoute} />
                    <div className="main-content-inner">
                        <div className="container-fluid">
                            <div className="row">
                                {this.props.children}
                            </div>
                        </div>
                    </div>
                    <BlockingModal/>
                </div>
            </>
        )
    }
}

export default Layout;
