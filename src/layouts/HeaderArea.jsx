import React from "react";
import {AppContext} from "../components/App/AppContext";
import {
    FaColumns,
    FaSync,
    FaStream,
    FaBars,
    FaArchive
} from "react-icons/fa"
import BrowserTabsApi from "../infrastructure/Api/BrowserTabsApi";
import {isUserPremium} from "../infrastructure/helpers";
import {Dropdown, DropdownButton} from "react-bootstrap";
import {errorToast} from "../infrastructure/toast";

class HeaderArea extends React.Component {
    static contextType = AppContext;

    liveSyncRoute = () => {
        if (!isUserPremium(this.context.user)) {
            errorToast(this.context.t('feature-is-available-only-for-paid-subscription'), 5000);

            return false;
        }

        this.props.setRoute('/live-synchronization');
    }

    render() {
        const t = this.context.t;
        const browserTabsApi = new BrowserTabsApi();

        return (
            <div className="mainheader-area">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-12 clearfix text-right px-0">
                            <div className={'row align-items-center'}>
                                <div className={'col-4 text-center pr-0'}>
                                    <DropdownButton className={'menu-dropdown'} variant={'light'} title={
                                        <>
                                            <FaBars />  {this.context.t('Menu')}
                                        </>
                                    }>
                                        <Dropdown.Item as={'button'} onClick={() => {this.props.setRoute('/sessions')}}>
                                            <FaStream/>
                                            <span className={'ml-05'}>{this.context.t('sessions')}</span>
                                        </Dropdown.Item>
                                        <Dropdown.Item as={'button'} onClick={this.liveSyncRoute}>
                                            <FaSync/>
                                            <span className={'ml-05'}>{this.context.t('live-synchronization')}</span>
                                        </Dropdown.Item>
                                        <Dropdown.Item as={'button'} onClick={() => {this.props.setRoute('/archived-tabs')}}>
                                            <FaArchive />
                                            <span className={'ml-05'}>{this.context.t('Archive')}</span>
                                        </Dropdown.Item>
                                        <Dropdown.Divider />
                                        <Dropdown.Item onClick={() => {browserTabsApi.createTab(process.env.REACT_APP_PRIVATE_USER_HOST + '/sessions')}} as={'button'}>
                                            <FaColumns/>
                                            <span className={'ml-05'}>{this.context.t('dashboard')}</span>
                                        </Dropdown.Item>
                                        {/*<Dropdown.Item onClick={() => {browserTabsApi.createTab(process.env.REACT_APP_PRIVATE_USER_HOST + '/referrals')}} as={'button'}>*/}
                                        {/*    <FaTags/>*/}
                                        {/*    <span className={'ml-05'}>{this.context.t('referrals')}</span>*/}
                                        {/*</Dropdown.Item>*/}
                                        <Dropdown.Divider />
                                        <Dropdown.Item onClick={() => {browserTabsApi.createTab(process.env.REACT_APP_SITE_HOST + '/logout')}} as={'button'}>
                                            {this.context.t('logout')}
                                        </Dropdown.Item>
                                        <Dropdown.Divider />
                                        <div className={'alert mb-0'}>
                                            {this.context.t('some-of-possible-features-are-available-only')}
                                            <a onClick={() => {browserTabsApi.createTab(process.env.REACT_APP_PRIVATE_USER_HOST + '/sessions')}} href={'#'}> {this.context.t('web-platform')}</a>.
                                        </div>
                                    </DropdownButton>
                                </div>
                                <div className={'col-8 px-0'}>
                                    <div className="user-profile m-0">
                                        <h6 className="user-name">
                                            {this.context.user.email}
                                            <span className={`ml-05 fw-500`}>
                                                ({isUserPremium(this.context.user) ? t('Premium') : t('Basic')})
                                            </span>
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default HeaderArea;