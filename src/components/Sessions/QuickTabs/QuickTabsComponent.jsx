import React from "react";
import QuickTabList from "./QuickTabList";
import {AppContext} from "../../App/AppContext";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
import {
    FaEllipsisV,
    FaFire,
    FaPlus,
    FaPlusCircle,
} from "react-icons/fa";
import {Dropdown, DropdownButton} from "react-bootstrap";
import apiClient from "../../../infrastructure/Api/ApiClient";
import {filterByEnableStatus} from "../../../infrastructure/helpers";
import LoadMore from "../../../infrastructure/Elements/LoadMore";
import BrowserWindowsApi from "../../../infrastructure/Api/BrowserWindowsApi";
import BrowserTabsApi from "../../../infrastructure/Api/BrowserTabsApi";
import TabFactory from "../../../infrastructure/Factory/TabFactory";
import {successToast} from "../../../infrastructure/toast";

class QuickTabsComponent extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            tabs: [],
            page: 1,
            perPage: 20,
            nextPage: null,
        }
    }

    fetchTabs = () => {
        apiClient(this.context.token, this.context.deviceId).get(`/quick-tabs?&per_page=${this.state.perPage}&page=${this.state.page}`)
            .then(res => {
                let tabs = [...this.state.tabs, ...res.data];

                this.setState({
                    page: this.state.page + 1,
                    nextPage: res.headers['x-pagination-next-page'] === 'null' ? null : Number(res.headers['x-pagination-next-page']),
                    tabs: tabs,
                });

                if (this.state.page > 2) {
                    window.scrollTo(0, document.body.scrollHeight);
                }
            })
    }

    componentDidMount() {
        this.fetchTabs();
    }

    loadMore = () => {
        if (null !== this.state.nextPage) {
            this.fetchTabs();
        }
    }

    setTabs = (tabs) => {
        this.setState({tabs: tabs});
    }

    getTabs = () => {
        return this.state.tabs;
    }

    addFocusedTab = () => {
        new BrowserWindowsApi().findFocused((focusedWindow) => {
            new BrowserTabsApi().findByWindow(focusedWindow.id, (tabs) => {
                const activeTab = tabs.find(t => t.active === true);
                const tab = new TabFactory(activeTab).json();

                apiClient(this.context.token, this.context.deviceId).post('/quick-tabs', {tabs: [tab]}).then(tabsResponse => {
                    successToast(this.context.t('successfully-saved'));
                    this.setTabs([...this.state.tabs, ...tabsResponse.data]);
                    window.scrollTo(0,document.body.scrollHeight);
                })
            })
        })
    }

    addWindowTabs = () => {
        new BrowserWindowsApi().findFocused((focusedWindow) => {
            new BrowserTabsApi().findByWindow(focusedWindow.id, (tabs) => {
                const normalizedTabs = TabFactory.createMany(tabs);

                apiClient(this.context.token, this.context.deviceId).post('/quick-tabs', {tabs: normalizedTabs}).then(tabsResponse => {
                    successToast(this.context.t('successfully-saved'));
                    this.setTabs([...this.state.tabs, ...tabsResponse.data]);
                    window.scrollTo(0,document.body.scrollHeight);
                })
            })
        })
    }

    render() {
        const [enabledTabs, disabledTabs] = filterByEnableStatus(this.state.tabs);
        const hasDisabledTabs = disabledTabs.length > 0;

        return (
            <>
                <div className={'container mt-2'}>
                    <div className={'row'}>
                        <div className={'col-12'}>
                            <div className={'according'}>
                                <div className={'card shadow mt-2'}>
                                    <div className={'card-header text-center synced-device-tabs-header'}>
                                        <div className={`title text-left`}>
                                            <span className={'session-name'}>
                                                <span><FaFire /></span>
                                                <span className={'ml-05'}>{this.context.t('quick-tabs')}</span>
                                                {hasDisabledTabs &&
                                                    <div className={'ml-05 text-danger'}>{this.context.t('disabled-quick-tabs')}</div>
                                                }
                                            </span>
                                        </div>
                                        <div className={`action text-center`}>
                                            <ButtonGroup className={'ml-15'}>
                                                <Button data-bs-toggle="tooltip" data-bs-placement="top" title={this.context.t('add-focused-tab')} onClick={this.addFocusedTab} size={'sm'} variant={'transparent'}>
                                                    <FaPlus/>
                                                </Button>
                                                <Button data-bs-toggle="tooltip" data-bs-placement="top" title={this.context.t('add-focused-window')} onClick={this.addWindowTabs} size={'sm'} variant={'transparent'}>
                                                    <FaPlusCircle/>
                                                </Button>
                                                <DropdownButton className={'no-dropdown-border'} variant={'transparent'} as={ButtonGroup} title={<FaEllipsisV />}>
                                                    <Dropdown.Item>
                                                        <FaPlus onClick={this.addFocusedTab}/>
                                                        <span className={'ml-05 fw-500'}>{this.context.t('add-focused-tab')}</span>
                                                    </Dropdown.Item>
                                                    <Dropdown.Item onClick={this.addWindowTabs}>
                                                        <FaPlusCircle/>
                                                        <span className={'ml-05 fw-500'}>{this.context.t('add-focused-window')}</span>
                                                    </Dropdown.Item>
                                                </DropdownButton>
                                            </ButtonGroup>
                                        </div>
                                    </div>
                                    <div className={'card-body p-0'}>
                                        <QuickTabList getTabs={this.getTabs} setTabs={this.setTabs} restrictions={this.props.restrictions}/>
                                        {null !== this.state.nextPage &&
                                            <div className={'container mb-2'}>
                                                <LoadMore loadMore={this.loadMore}/>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default QuickTabsComponent;