import React from "react";
import arrayMove from "array-move";
import apiClient from "../../../infrastructure/Api/ApiClient";
import {makeArrayOfIds} from "../../../infrastructure/helpers";
import {SortableContainer, SortableElement} from "react-sortable-hoc";
import QuickTab from "./QuickTab";
import {AppContext} from "../../App/AppContext";

class SortableQuickTabsList extends React.Component {
    static contextType = AppContext;

    onSortEnd = ({oldIndex, newIndex}) => {
        if (oldIndex !== newIndex) {
            const newOrder = arrayMove(this.props.tabs, oldIndex, newIndex);

            apiClient(this.context.token, this.context.deviceId).put(`/quick-tabs`, {'tabs': makeArrayOfIds(newOrder)}).then(() => {
                let tabs = this.props.tabs;

                for (let i = 0; i < tabs.length; i++) {
                    if (tabs[i]['is_enabled']) {
                        tabs.splice(i, 1);
                        i--;
                    }
                }

                const tabsWithNewOrder = [...newOrder, ...tabs];
                this.props.setTabs(tabsWithNewOrder);
            })
        }
    };

    render() {
        const SortableItem = SortableElement(({tab}) => {
                return (
                    <>
                        <QuickTab
                            tab={tab}
                            getTabs={this.props.getTabs}
                            setTabs={this.props.setTabs}
                            restrictions={this.props.restrictions}
                        />
                    </>
                )
            }
        );

        const SortableList = SortableContainer(({tabs}) => {
            return (
                <ul className="list-group synced-device-tabs">
                    {
                        tabs.map((tab, index) => (
                            <SortableItem key={`tab-${tab.id}`} index={index} tab={tab}/>
                        ))
                    }
                </ul>
            );
        });

        return <SortableList axis="y" pressDelay={150} tabs={this.props.tabs} onSortEnd={this.onSortEnd}/>
    }
}

export default SortableQuickTabsList;