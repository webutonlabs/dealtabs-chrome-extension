import React from "react";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import {CopyToClipboard} from "react-copy-to-clipboard/lib/Component";
import {FaCopy, FaTrash} from "react-icons/fa";
import {sliceString} from "../../../infrastructure/helpers";
import BrowserTabsApi from "../../../infrastructure/Api/BrowserTabsApi";
import {successToast} from "../../../infrastructure/toast";
import {AppContext} from "../../App/AppContext";
import apiClient from "../../../infrastructure/Api/ApiClient";
import TabIcon from "../../../elements/TabIcon";

class QuickTab extends React.Component {
    static contextType = AppContext;

    openTab = () => {
        new BrowserTabsApi().createTab(this.props.tab.url);
    }

    copied = () => {
        if (this.props.tab['is_enabled']) {
            successToast(this.context.t('Copied'));
        }
    }

    deleteTab = () => {
        apiClient(this.context.token, this.context.deviceId).delete(`/quick-tabs/${this.props.tab.id}`)
            .then(() => {
                const tabs = this.props.getTabs();

                for (let i = 0; i < tabs.length; i++) {
                    if (tabs[i].id === this.props.tab.id) {
                        tabs.splice(i, 1);
                        break;
                    }
                }

                const tabsLimit = this.props.restrictions.quickTabsLimit();
                tabs.forEach((tab, index) => {
                    if (typeof tabsLimit === 'number' && index <= tabsLimit) {
                        tab['is_enabled'] = true;
                    }
                });

                this.props.setTabs(tabs);
                successToast(this.context.t('successfully-deleted'));
            })
    }

    render() {
        const isEnabled = this.props.tab['is_enabled'];

        return (
            <li className="p-0 list-group-item d-flex justify-content-between align-items-center">
                <button onClick={this.openTab} className={`btn btn-link ${!isEnabled ? 'disabled' : ''}`} style={{padding: '5px 10px'}}>
                    <TabIcon src={this.props.tab.icon}/>
                    <span className={`ml-05 tab-name ${!isEnabled ? 'text-dark' : ''}`}>{sliceString(this.props.tab.name, 85)}</span>
                </button>
                <ButtonGroup data-bs-toggle="tooltip" data-bs-placement="top" title={this.context.t('Copy')} classname={'w-100'}>
                    <CopyToClipboard text={isEnabled ? this.props.tab.url : ''}>
                        <button disabled={!isEnabled} onClick={this.copied} className={`btn btn-light btn-sm`} style={{padding: '5px 10px'}}>
                            <FaCopy/>
                        </button>
                    </CopyToClipboard>
                    <button className={`text-danger btn btn-light btn-sm`} onClick={this.deleteTab} style={{padding: '5px 10px'}}>
                        <FaTrash/>
                    </button>
                </ButtonGroup>
            </li>
        )
    }
}

export default QuickTab;