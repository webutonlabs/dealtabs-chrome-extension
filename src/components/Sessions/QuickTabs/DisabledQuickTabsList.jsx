import React from "react";
import QuickTab from "./QuickTab";

class DisabledQuickTabsList extends React.Component {
    render() {
        return (
            <ul className="list-group synced-device-tabs">
                {this.props.tabs.map(tab => {
                    return (
                        <QuickTab
                            key={tab.id}
                            tab={tab}
                            getTabs={this.props.getTabs}
                            setTabs={this.props.setTabs}
                            restrictions={this.props.restrictions}
                        />
                    )
                })}
            </ul>
        )
    }
}

export default DisabledQuickTabsList;