import React from "react";
import {AppContext} from "../../App/AppContext";
import {filterByEnableStatus, isUserPremium} from "../../../infrastructure/helpers";
import SortableQuickTabsList from "./SortableQuickTabsList";
import DisabledQuickTabsList from "./DisabledQuickTabsList";

class QuickTabList extends React.Component {
    static contextType = AppContext;

    render() {
        let sortableTabs, notSortableTabs = [];

        if (isUserPremium(this.context.user)) {
            sortableTabs = this.props.getTabs();
        } else {
            let [enabledTabs, disabledTabs] = filterByEnableStatus(this.props.getTabs());

            sortableTabs = enabledTabs;
            notSortableTabs = disabledTabs;
        }

        return <>
            <SortableQuickTabsList
                tabs={sortableTabs}
                getTabs={this.props.getTabs}
                setTabs={this.props.setTabs}
                restrictions={this.props.restrictions}
            />
            {notSortableTabs.length > 0 &&
                <DisabledQuickTabsList
                    tabs={notSortableTabs}
                    getTabs={this.props.getTabs}
                    setTabs={this.props.setTabs}
                    restrictions={this.props.restrictions}
                />
            }
        </>
    }
}

export default QuickTabList;