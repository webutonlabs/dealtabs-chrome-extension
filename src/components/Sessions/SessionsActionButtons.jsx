import React from "react";
import {AppContext} from "../App/AppContext";

class SessionsActionButtons extends React.Component {
    static contextType = AppContext;

    render() {
        const t = this.context.t;

        return (
            <div className={'row justify-content-left mt-10'}>
                <div className="invoice-buttons text-center ml-10">
                    <button onClick={this.props.saveNewWithCurrentWindow} className={'invoice-btn btn text-white-hover'}>
                        {t('save-new')}
                    </button>
                    <button onClick={this.props.saveNewWithAllWindows} className={'invoice-btn btn text-white-hover'}>
                        {t('save-new-all')}
                    </button>
                    <button onClick={this.props.newEmpty} className={'invoice-btn btn text-white-hover'}>
                        {t('new-empty')}
                    </button>
                    {/*<span className={'total-sessions'}>{this.context.t('total-sessions')}: {this.props.totalSessions}</span>*/}
                </div>
            </div>
        )
    }
}

export default SessionsActionButtons;