import React from "react";
import {sliceString} from "../../../infrastructure/helpers";
import BrowserTabsApi from "../../../infrastructure/Api/BrowserTabsApi";
import {FaCopy, FaTrash} from "react-icons/fa"
import {CopyToClipboard} from "react-copy-to-clipboard/lib/Component";
import {AppContext} from "../../App/AppContext";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import apiClient from "../../../infrastructure/Api/ApiClient";
import {successToast} from "../../../infrastructure/toast";
import TabIcon from "../../../elements/TabIcon";

class WindowTabItem extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {tabCopied: false}
    }

    toggleTabCopied = () => {
        this.setState({tabCopied: true});

        setTimeout(() => {
            this.setState({tabCopied: false})
        }, 2000)
    }


    openTab = () => {
        new BrowserTabsApi().createTab(this.props.tab.url);
    }

    deleteTab = () => {
        this.setState({isLoading: true});

        apiClient(this.context.token, this.context.deviceId).delete(`/window-tabs/${this.props.tab.id}`)
            .then(() => {
                const sessions = this.props.getSessions();

                for (let i = 0; i < sessions.length; i++) {
                    if (sessions[i].id === this.props.session.id) {
                        for (let j = 0; j < sessions[i].windows.length; j++) {
                            if (sessions[i].windows[j].id === this.props.window.id) {
                                for (let k = 0; k < sessions[i].windows[j].tabs.length; k++) {
                                    if (sessions[i].windows[j].tabs[k].id === this.props.tab.id) {
                                        sessions[i].windows[j].tabs.splice(k, 1);
                                        break;
                                    }
                                }

                                const tabsLimit = this.props.restrictions.tabsLimit();
                                sessions[i].windows[j].tabs.forEach((tab, index) => {
                                    if (typeof tabsLimit === 'number' && index < tabsLimit) {
                                        tab['is_enabled'] = true;
                                    }
                                })

                                break;
                            }
                        }
                        break;
                    }
                }

                this.props.updateSessions(sessions);
                this.setState({isLoading: false});

                successToast(this.context.t('successfully-deleted'));
            })
    }

    render() {
        const t = this.context.t;
        const tabName = this.props.tab.name === null ? this.props.tab.url : this.props.tab.name;
        const isEnabled = this.props.tab['is_enabled'] && this.props.window['is_enabled'];

        return (
            <li id={`window-tab-${this.props.tab.id}`} className="p-0 list-group-item d-flex justify-content-between align-items-center">
                <button onClick={this.openTab} className={`btn btn-link ${!isEnabled ? 'disabled' : ''}`}>
                    <TabIcon src={this.props.tab.icon}/>
                    <span className={`ml-05 tab-name ${!isEnabled ? 'text-dark' : ''}`}>{sliceString(tabName, 60)}</span>
                </button>
                <ButtonGroup data-bs-toggle="tooltip" data-bs-placement="top" title={t('Copy')} classname={'w-100'}>
                    <CopyToClipboard text={isEnabled ? this.props.tab.url : ''} onCopy={isEnabled ? this.toggleTabCopied : () => {}}>
                        <button disabled={!isEnabled} className={`btn btn-light ${!isEnabled ? 'disabled' : ''}`}>
                            {this.state.tabCopied ? t('Copied') : <FaCopy/>}
                        </button>
                    </CopyToClipboard>
                    <button className={`text-danger btn btn-light`} onClick={this.deleteTab}>
                        <FaTrash/>
                    </button>
                </ButtonGroup>
            </li>
        )
    }
}

export default WindowTabItem;