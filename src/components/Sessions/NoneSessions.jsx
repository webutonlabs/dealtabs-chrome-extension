import React from "react";
import AppLoader from "../../elements/Loader/AppLoader";
import BrowserWindowsApi from "../../infrastructure/Api/BrowserWindowsApi";
import BrowserTabsApi from "../../infrastructure/Api/BrowserTabsApi";
import {AppContext} from "../App/AppContext";
import TabFactory from "../../infrastructure/Factory/TabFactory";
import WindowFactory from "../../infrastructure/Factory/WindowFactory";
import SessionFactory from "../../infrastructure/Factory/SessionFactory";
import apiClient from "../../infrastructure/Api/ApiClient";

class NoneSessions extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {isLoading: false}
    }

    saveSession = () => {
        this.setState({isLoading: true});

        const browserWindowsApi = new BrowserWindowsApi();
        const browserTabsApi = new BrowserTabsApi();

        browserWindowsApi.findFocused((focusedWindow) => {
            browserTabsApi.findByWindow(focusedWindow.id, (tabs) => {
                const normalizedTabs = TabFactory.createMany(tabs);
                const window = new WindowFactory(normalizedTabs).json();
                const session = new SessionFactory(null, [window]).json();

                apiClient(this.context.token, this.context.deviceId).post('/sessions', session).then(sessionResponse => {
                    this.props.updateSessions([sessionResponse.data]);
                    this.props.incrementTotalSessions();

                    this.setState({isLoading: false});
                })
            })
        })
    }

    render() {
        const t = this.context.t;

        return (
            this.state.isLoading ? <AppLoader/> :
                <div className={'row justify-content-center mt-10 text-center'}>
                    <div className={'col-12 mt-10'}>
                        <div className="invoice-buttons text-center">
                            <button onClick={this.saveSession} className={'invoice-btn btn text-white-hover'}>
                                {t('save-your-first-session')}
                            </button>
                        </div>
                    </div>
                </div>
        )
    }
}

export default NoneSessions;