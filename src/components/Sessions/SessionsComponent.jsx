import React from "react";
import SessionItem from "./Session/SessionItem";
import {AppContext} from "../App/AppContext";
import AppLoader from "../../elements/Loader/AppLoader";
import NoneSessions from "./NoneSessions";
import BrowserWindowsApi from "../../infrastructure/Api/BrowserWindowsApi";
import BrowserTabsApi from "../../infrastructure/Api/BrowserTabsApi";
import TabFactory from "../../infrastructure/Factory/TabFactory";
import WindowFactory from "../../infrastructure/Factory/WindowFactory";
import SessionFactory from "../../infrastructure/Factory/SessionFactory";
import apiClient from "../../infrastructure/Api/ApiClient";
import {successToast} from "../../infrastructure/toast";
import {MembershipRestrictions} from "../../infrastructure/Membership/MembershipRestrictions";
import SessionsActionButtons from "./SessionsActionButtons";
import LoadMore from "../../infrastructure/Elements/LoadMore";
import QuickTabsComponent from "./QuickTabs/QuickTabsComponent";

class SessionsComponent extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            sessions: [],
            perPage: 5,
            page: 1,
            nextPage: null,
            restrictions: new MembershipRestrictions([]),
            totalSessions: 0
        }
    }

    componentDidMount() {
        this.fetchSessions();

        apiClient(this.context.token, this.context.deviceId).get('/parameters/memberships').then(res => {
            this.setState({restrictions: new MembershipRestrictions(res.data).forUser(this.context.user)});
        });
    }

    loadMore = () => {
        if (null !== this.state.nextPage) {
            this.fetchSessions();
        }
    }

    fetchSessions = () => {
        this.setState({isLoading: true});

        apiClient(this.context.token, this.context.deviceId).get(`/sessions?relations=windows,tabs&per_page=${this.state.perPage}&page=${this.state.page}`)
            .then(res => {
                let sessions = [...this.state.sessions, ...res.data];

                this.setState({
                    isLoading: false,
                    page: this.state.page + 1,
                    nextPage: res.headers['x-pagination-next-page'] === 'null' ? null : Number(res.headers['x-pagination-next-page']),
                    sessions: sessions,
                    totalSessions: res.headers['x-pagination-total']
                });

                if (this.state.page > 2) {
                    window.scrollTo(0, document.body.scrollHeight);
                }
            })
    }

    updateSessions = (sessions) => {
        this.setState({sessions: sessions});
    }

    getSessions = () => {
        return this.state.sessions;
    }

    saveNewWithCurrentWindow = () => {
        this.setState({isLoading: true});

        const browserWindowsApi = new BrowserWindowsApi();
        const browserTabsApi = new BrowserTabsApi();

        browserWindowsApi.findFocused((focusedWindow) => {
            browserTabsApi.findByWindow(focusedWindow.id, (tabs) => {
                const normalizedTabs = TabFactory.createMany(tabs);
                const window = (new WindowFactory(normalizedTabs)).json();
                const session = (new SessionFactory(null, [window])).json();

                apiClient(this.context.token, this.context.deviceId)
                    .post('/sessions', session)
                    .then(this.sessionCreatedCallback)
            })
        })
    }

    saveNewWithAllWindows = () => {
        this.setState({isLoading: true});

        const browserTabsApi = new BrowserTabsApi();
        browserTabsApi.getStructuredByWindows((structuredTabs) => {
            const windows = new WindowFactory().createManyFromStructuredTabs(structuredTabs);
            const session = new SessionFactory(null, windows).json();

            apiClient(this.context.token, this.context.deviceId)
                .post('/sessions', session)
                .then(this.sessionCreatedCallback);
        });
    }

    newEmpty = () => {
        let session = new SessionFactory(null, []).json();

        apiClient(this.context.token, this.context.deviceId)
            .post('/sessions', session)
            .then(this.sessionCreatedCallback)
    }

    sessionCreatedCallback = (axiosResponse) => {
        let sessions = this.state.sessions;
        sessions.unshift(axiosResponse.data);

        const sessionLimit = this.state.restrictions.sessionLimit();
        if (typeof sessionLimit === 'number' && sessions.length > sessionLimit) {
            sessions[sessionLimit]['is_enabled'] = false;
        }

        this.setState({sessions: sessions, isLoading: false});
        this.incrementTotalSessions();

        successToast(this.context.t('successfully-saved'));
    }

    setPage = (page) => {
        this.setState({page: page});
    }

    incrementTotalSessions = () => {
        this.setState({total: ++this.state.totalSessions})
    }

    decrementTotalSessions = () => {
        let decremented = --this.state.totalSessions;
        if (decremented < 0) {
            decremented = 0;
        }

        this.setState({total: decremented});
    }

    render() {
        return (
            this.state.isLoading ? <AppLoader/> :
                <>
                    <div className={'container'}>
                        {this.state.sessions.length === 0 ? <NoneSessions
                                incrementTotalSessions={this.incrementTotalSessions}
                                decrementTotalSessions={this.decrementTotalSessions}
                                updateSessions={this.updateSessions}
                            /> :
                            <>
                                <SessionsActionButtons
                                    saveNewWithCurrentWindow={this.saveNewWithCurrentWindow}
                                    saveNewWithAllWindows={this.saveNewWithAllWindows}
                                    newEmpty={this.newEmpty}
                                    totalSessions={this.state.totalSessions}
                                />
                                <>
                                    {this.state.sessions.map(session => (<SessionItem
                                        updateSessions={this.updateSessions}
                                        getSessions={this.getSessions}
                                        session={session}
                                        key={session.id}
                                        setPage={this.setPage}
                                        fetchSessions={this.fetchSessions}
                                        restrictions={this.state.restrictions}
                                        incrementTotalSessions={this.incrementTotalSessions}
                                        decrementTotalSessions={this.decrementTotalSessions}
                                    />))}
                                </>
                                {null !== this.state.nextPage && <LoadMore loadMore={this.loadMore}/>}
                            </>
                        }
                    </div>
                    <QuickTabsComponent restrictions={this.state.restrictions}/>
                </>
        )
    }
}

export default SessionsComponent;