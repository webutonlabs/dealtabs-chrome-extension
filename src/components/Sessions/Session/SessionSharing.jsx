import React from "react";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import {CopyToClipboard} from "react-copy-to-clipboard/lib/Component";
import Button from "react-bootstrap/Button";
import {MdContentCopy} from "react-icons/md";
import {AppContext} from "../../App/AppContext";

class SessionSharing extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {shareLinkCopied: false}
    }

    toggleShareLinkCopied = () => {
        this.setState({shareLinkCopied: true});

        setTimeout(() => {
            this.setState({shareLinkCopied: false})
        }, 2000)
    }

    render() {
        const t = this.context.t;

        return (
            <div className={'row mt-10 mb-10 justify-content-center text-center'}>
                <div className={'container'}>
                    <input className={'form-control'} type={'text'}
                           value={`${process.env.REACT_APP_SITE_HOST}/${this.context.user.locale}/shared-session/${this.props.shareRef}`}/>
                </div>
                <div className={'mt-10'}>
                    <ButtonGroup aria-label="First group" classname={'w-100'}>
                        <CopyToClipboard text={`${process.env.REACT_APP_SITE_HOST}/${this.context.user.locale}/shared-session/${this.props.shareRef}`} onCopy={this.toggleShareLinkCopied}>
                            <Button variant={'light'}><MdContentCopy/><span className={'ml-05 fw-500'}>{this.state.shareLinkCopied ? t('Copied') : t('Copy')}</span></Button>
                        </CopyToClipboard>
                    </ButtonGroup>
                </div>
            </div>
        );
    }
}

export default SessionSharing;