import React from "react";
import {Collapse} from "react-bootstrap";
import Button from "react-bootstrap/Button";
import {MdCancel, MdCheck} from "react-icons/md";
import SessionWindowItem from "../SessionWindow/SessionWindowItem";
import BrowserWindowsApi from "../../../infrastructure/Api/BrowserWindowsApi";
import BrowserTabsApi from "../../../infrastructure/Api/BrowserTabsApi";
import TabFactory from "../../../infrastructure/Factory/TabFactory";
import WindowFactory from "../../../infrastructure/Factory/WindowFactory";
import AppLoader from "../../../elements/Loader/AppLoader";
import {AppContext} from "../../App/AppContext";
import {filterByEnableStatus, isUserPremium} from "../../../infrastructure/helpers";
import apiClient from "../../../infrastructure/Api/ApiClient";
import {errorToast, successToast} from "../../../infrastructure/toast";
import SessionSharing from "./SessionSharing";
import SessionActionsDropdown from "./SessionActionsDropdown";
import CollaboratedUsersModal from "../Collaboration/CollaboratedUsersModal";
import AddCollaboratorModal from "../Collaboration/AddCollaboratorModal";

class SessionItem extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            windowsCollapsed: false,
            isLoading: false,
            onShare: false,
            shareReference: null,
            onEdit: false,
            editForm: {
                valid: true,
                touched: false,
                invalidMessage: '',
                value: props.session.name
            },
            showCollaboratedUsersModal: false,
            showAddCollaboratorModal: false,
            collaboratedUsers: []
        }
    }

    toggleOnShare = () => {
        if (this.state.shareReference === null) {
            this.setState({isLoading: true});

            apiClient(this.context.token, this.context.deviceId).post(`/sessions/${this.props.session.id}/share`, {})
                .then(res => {
                    this.setState({
                        shareReference: res.data['reference_id'],
                        onShare: true,
                        isLoading: false,
                        windowsCollapsed: true
                    })
                })
        } else {
            this.setState({onShare: !this.state.onShare, windowsCollapsed: true});
        }
    }

    toggleWindows = () => {
        this.setState({windowsCollapsed: !this.state.windowsCollapsed});
    }

    addWindows = () => {
        this.setState({isLoading: true});

        new BrowserTabsApi().getStructuredByWindows((structuredTabs) => {
            const windows = new WindowFactory().createManyFromStructuredTabs(structuredTabs);

            apiClient(this.context.token, this.context.deviceId).post('/session-windows', {session: this.props.session.id, windows: windows}).then(windowsResponse => {
                let sessions = this.props.getSessions();

                for (let i = 0; i < sessions.length; i++) {
                    if (sessions[i].id === this.props.session.id) {
                        windowsResponse.data.forEach(savedWindow => {
                            sessions[i].windows.push(savedWindow);
                        })

                        break;
                    }
                }

                this.props.updateSessions(sessions);
                this.setState({isLoading: false, windowsCollapsed: true});

                document
                    .getElementById(`session-window-${windowsResponse.data[windowsResponse.data.length - 1].id}`)
                    .scrollIntoView({behavior: 'smooth'});
            })
        })
    }

    addFocusedWindow = () => {
        this.setState({isLoading: true});

        new BrowserWindowsApi().findFocused((focusedWindow) => {
            new BrowserTabsApi().findByWindow(focusedWindow.id, (tabs) => {
                let window = new WindowFactory(TabFactory.createMany(tabs)).json();

                apiClient(this.context.token, this.context.deviceId).post('/session-windows', {session: this.props.session.id, windows: [window]}).then(windowsResponse => {
                    let sessions = this.props.getSessions();

                    for (let i = 0; i < sessions.length; i++) {
                        if (sessions[i].id === this.props.session.id) {
                            windowsResponse.data.forEach(savedWindow => {
                                sessions[i].windows.push(savedWindow);
                            })

                            break;
                        }
                    }

                    this.props.updateSessions(sessions);
                    this.setState({isLoading: false, windowsCollapsed: true});

                    document
                        .getElementById(`session-window-${windowsResponse.data[windowsResponse.data.length - 1].id}`)
                        .scrollIntoView({behavior: 'smooth'});
                })
            })
        })
    }

    openSession = () => {
        const [enabledWindows] = filterByEnableStatus(this.props.session.windows);
        new BrowserWindowsApi().openWindows(enabledWindows);
    }

    editSession = () => {
        this.setState({isLoading: true});

        apiClient(this.context.token, this.context.deviceId).patch(`/sessions/${this.props.session.id}`, {name: this.state.editForm.value})
            .then(res => {
                const sessions = this.props.getSessions();

                for (let i = 0; i < sessions.length; i++) {
                    if (sessions[i].id === this.props.session.id) {
                        res.data.windows.sort((a, b) => (a['order_index'] > b['order_index']) ? 1 : -1);
                        res.data.windows.forEach(window => {
                            window.tabs.sort((a, b) => (a['order_index'] > b['order_index']) ? 1 : -1);
                        })

                        sessions[i] = res.data;

                        break;
                    }
                }

                this.props.updateSessions(sessions);
                this.setState({isLoading: false, onEdit: false});

                successToast(this.context.t('successfully-saved'));
            })
    }

    handleEdit = (e) => {
        const form = this.state.editForm;
        form.touched = true;
        form.value = e.target.value;

        if (form.value.length === 0) {
            form.valid = false;
            form.invalidMessage = this.context.t('session-name-cannot-be-empty');
        } else {
            form.valid = true;
        }

        this.setState({editForm: form});
    }

    toggleEdit = () => {
        const form = this.state.editForm;
        form.touched = false;
        form.valid = true;
        form.value = this.props.session.name;

        this.setState({onEdit: !this.state.onEdit, editForm: form});
    }

    deleteSession = () => {
        this.setState({isLoading: true});

        if (this.props.session.owned) {
            apiClient(this.context.token, this.context.deviceId).delete(`/sessions/${this.props.session.id}`)
                .then(() => {
                    const sessions = this.props.getSessions();
                    sessions.forEach((session, index) => {
                        if (this.props.session.id === session.id) {
                            sessions.splice(index, 1);
                        }
                    });

                    /**
                     * For ex. user has got 10 sessions. We configured 5 sessions per page.
                     * If user will delete 5 sessions, then SessionsComponent's state sessions property results in empty array so
                     * NoneSessions component will load which results in bad user experience because user sees 'None sessions message' when he actually may have some.
                     * To prevent the above problem we try to load sessions from the first page
                     */
                    if (sessions.length === 0) {
                        this.props.setPage(1);
                        this.props.fetchSessions();
                    }

                    this.props.updateSessions(sessions);
                    this.setState({isLoading: false});
                    this.props.decrementTotalSessions();

                    successToast(this.context.t('successfully-deleted'));
                })
        } else {
            apiClient(this.context.token, this.context.deviceId)
                .patch(`/collaboration/delete-collaborator`, {email: this.context.user.email, session: this.props.session.id}).then(res => {
                const sessions = this.props.getSessions();
                for (let i = 0; i < sessions.length; i++) {
                    if (sessions[i].id === this.props.session.id) {
                        sessions.splice(i, 1);
                        break;
                    }
                }

                this.props.updateSessions(sessions);
                successToast(this.context.t('successfully-deleted'));
            })
        }
    }

    setCollaboratedUsers = (users) => {
        this.setState({collaboratedUsers: users});
    }

    toggleCollaboratedUsersModal = () => {
        const show = !this.state.showCollaboratedUsersModal;

        if (show) {
            apiClient(this.context.token, this.context.deviceId).get(`/collaboration?session=${this.props.session.id}`).then(res => {
                this.setCollaboratedUsers(res.data);
            });
        }

        this.setState({showCollaboratedUsersModal: show});
    }

    toggleAddCollaboratorModal = () => {
        this.setState({showAddCollaboratorModal: !this.state.showAddCollaboratorModal});
    }

    toggleCollaboration = () => {
        if (!isUserPremium(this.context.user)) {
            errorToast(this.context.t('premium-feature'))
            return;
        }

        apiClient(this.context.token, this.context.deviceId).patch('/collaboration', {session: this.props.session.id}).then(res => {
            const sessions = this.props.getSessions();
            for (let i = 0; i < sessions.length; i++) {
                if (this.props.session.id === sessions[i].id) {
                    sessions[i]['enabled_for_collaboration'] = !sessions[i]['enabled_for_collaboration'];
                    break;
                }
            }

            this.props.updateSessions(sessions);
        })
    }

    render() {
        const t = this.context.t;
        const sessionLimit = this.props.restrictions.sessionLimit();

        let isEnabled = this.props.session['is_enabled'];
        if (typeof sessionLimit === 'number' && sessionLimit < this.props.session['order_index']) {
            isEnabled = false;
        }

        const isCollaborated = (this.props.session['enabled_for_collaboration'] || !this.props.session.owned)

        return (
            this.state.isLoading ? <AppLoader/> :
                <>
                    <div className="card mt-10 col-12 px-0">
                        <div className="according">
                            <div className="card shadow-sm">
                                <div className="card-header text-center">
                                    {this.state.onEdit ?
                                        <div className={'title-edit text-center'}>
                                            <div className={'container'}>
                                                <div className={'row justify-content-center'}>
                                                    <input
                                                        className={`form-control fw-500 
                                                    ${this.state.editForm.touched ? this.state.editForm.valid ? 'is-valid' : 'is-invalid' : ''}`}
                                                        value={this.state.editForm.value}
                                                        onChange={this.handleEdit}
                                                    />
                                                    {this.state.editForm.touched && !this.state.editForm.valid &&
                                                    <div className="invalid-feedback">
                                                        {this.state.editForm.invalidMessage}
                                                    </div>
                                                    }
                                                </div>
                                            </div>
                                        </div> :
                                        <div className={`title`}>
                                            <a className={`card-link ${isEnabled ? '' : 'text-dark'}`} onClick={isEnabled ? this.toggleWindows : () => {}}>
                                                <span className={'session-name'}>{this.props.session.name}</span>
                                                {isCollaborated &&
                                                    <span className={'shared-msg badge badge-primary ml-2'}>{this.context.t('collaborated')}</span>
                                                }
                                                {!isEnabled && <div className={'text-danger mt-10 disabled-session'}>{t('disabled-session')}</div>}
                                            </a>
                                        </div>
                                    }
                                    {this.state.onEdit ?
                                        <div className={'action'}>
                                            {this.state.editForm.valid &&
                                            <Button size={'sm'} onClick={this.editSession} variant={'transparent'}><MdCheck/></Button>
                                            }
                                            <Button className={'text-danger'} size={'sm'} onClick={this.toggleEdit} variant={'transparent'}><MdCancel/></Button>
                                        </div> :
                                        <SessionActionsDropdown
                                            session={this.props.session}
                                            isEnabled={isEnabled}
                                            toggleEdit={this.toggleEdit}
                                            openSession={this.openSession}
                                            toggleOnShare={this.toggleOnShare}
                                            addWindows={this.addWindows}
                                            addFocusedWindow={this.addFocusedWindow}
                                            deleteSession={this.deleteSession}
                                            toggleCollaboratedUsersModal={this.toggleCollaboratedUsersModal}
                                            collaborationEnabled={this.state.collaborationEnabled}
                                            toggleCollaboration={this.toggleCollaboration}
                                            toggleAddCollaboratorModal={this.toggleAddCollaboratorModal}
                                        />
                                    }
                                </div>
                                <Collapse in={this.state.windowsCollapsed}>
                                    <div className="card-body">
                                        {this.state.onShare && <SessionSharing shareRef={this.state.shareReference} />}
                                        {this.props.session.windows.length === 0 &&
                                        <h6 className={'text-info text-center mt-10'}>
                                            {this.context.t('none-saved-windows')}
                                        </h6>
                                        }
                                        {this.props.session.windows.map((window, index) => (<SessionWindowItem
                                            session={this.props.session}
                                            window={window}
                                            key={window.id}
                                            getSessions={this.props.getSessions}
                                            updateSessions={this.props.updateSessions}
                                            index={index}
                                            restrictions={this.props.restrictions}
                                        />))}
                                    </div>
                                </Collapse>
                            </div>
                        </div>
                    </div>
                    {this.props.session['enabled_for_collaboration'] &&
                        <>
                            <CollaboratedUsersModal
                                getSessions={this.props.getSessions}
                                updateSessions={this.props.updateSessions}
                                session={this.props.session}
                                show={this.state.showCollaboratedUsersModal}
                                toggleModal={this.toggleCollaboratedUsersModal}
                                users={this.state.collaboratedUsers}
                                setUsers={this.setCollaboratedUsers}
                            />
                            <AddCollaboratorModal
                                session={this.props.session}
                                show={this.state.showAddCollaboratorModal}
                                toggleModal={this.toggleAddCollaboratorModal}
                            />
                        </>
                    }
                </>
        )
    }
}

export default SessionItem;