import React from "react";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
import {MdEdit} from "react-icons/md";
import {Dropdown, DropdownButton} from "react-bootstrap";
import {
    FaEllipsisV,
    FaExternalLinkAlt,
    FaPlusCircle,
    FaPlusSquare,
    FaShare,
    FaTrash,
    FaUserPlus,
    FaUsers
} from "react-icons/fa";
import {AppContext} from "../../App/AppContext";
import {FaToggleOff, FaToggleOn} from "react-icons/all";
import {isUserPremium} from "../../../infrastructure/helpers";

class SessionActionsDropdown extends React.Component {
    static contextType = AppContext;

    render() {
        const t = this.context.t;
        const isEnabled = this.props.isEnabled;

        let isCollaborationEnabled = isEnabled;
        if (this.props.session.owned && (!isUserPremium(this.context.user) || !this.props.session['enabled_for_collaboration'])) {
            isCollaborationEnabled = false;
        }

        return (
            <div className={`action ${isEnabled ? '' : 'action-disabled'}`}>
                <ButtonGroup className={'ml-15'}>
                    <Button data-bs-toggle="tooltip" data-bs-placement="top" title={t('Open')} disabled={!isEnabled} size={'sm'} onClick={this.props.openSession} variant={'transparent'}><FaExternalLinkAlt/></Button>
                    <Button disabled={!isEnabled} size={'sm'} onClick={this.props.toggleEdit} variant={'transparent'}><MdEdit/></Button>
                    <DropdownButton className={'no-dropdown-border'} variant={'transparent'} as={ButtonGroup} title={<FaEllipsisV />}>
                        <Dropdown.Item disabled={!isEnabled} onClick={this.props.openSession}>
                            <FaExternalLinkAlt/>
                            <span className={'ml-05 fw-500'}>{t('Open')}</span>
                        </Dropdown.Item>
                        <Dropdown.Item disabled={!isEnabled} onClick={this.props.toggleOnShare}>
                            <FaShare/>
                            <span className={'ml-05 fw-500'}>{t('share-via-link')}</span>
                        </Dropdown.Item>
                        <Dropdown.Item disabled={!isEnabled} onClick={this.props.addWindows}>
                            <FaPlusSquare/>
                            <span className={'ml-05 fw-500'}>{t('Add windows')}</span>
                        </Dropdown.Item>
                        <Dropdown.Item disabled={!isEnabled} onClick={this.props.addFocusedWindow}>
                            <FaPlusCircle/>
                            <span className={'ml-05 fw-500'}>{t('add-focused-window')}</span>
                        </Dropdown.Item>
                        <Dropdown.Divider />
                        <Dropdown.Item disabled={!isCollaborationEnabled} onClick={this.props.toggleAddCollaboratorModal}>
                            <FaUserPlus />
                            <span className={'ml-05 fw-500'}>{this.context.t('add-collaborator')}</span>
                        </Dropdown.Item>
                        <Dropdown.Item disabled={!isCollaborationEnabled} onClick={this.props.toggleCollaboratedUsersModal}>
                            <FaUsers />
                            <span className={'ml-05 fw-500'}>{this.context.t('collaborated-users')}</span>
                        </Dropdown.Item>
                        {this.props.session.owned &&
                            <Dropdown.Item onClick={this.props.toggleCollaboration}>
                                {this.props.session['enabled_for_collaboration']
                                    ? <FaToggleOn className={'text-success'} />
                                    : <FaToggleOff className={'text-danger'} />
                                }
                                <span className={'ml-05 fw-500'}>{this.context.t('collaboration')}</span>
                            </Dropdown.Item>
                        }
                        <Dropdown.Divider />
                        <Dropdown.Item className={'text-danger'} onClick={this.props.deleteSession}>
                            <FaTrash/>
                            <span className={'ml-05'}>{this.context.t('delete')}</span>
                        </Dropdown.Item>
                    </DropdownButton>
                </ButtonGroup>
            </div>
        )
    }
}

export default SessionActionsDropdown;