import React from "react";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
import {
    FaEllipsisV,
    FaExchangeAlt,
    FaExternalLinkAlt,
    FaExternalLinkSquareAlt,
    FaObjectGroup,
    FaPlus,
    FaTrash
} from "react-icons/fa";
import WindowTabItem from "../WindowTab/WindowTabItem";
import BrowserWindowsApi from "../../../infrastructure/Api/BrowserWindowsApi";
import BrowserTabsApi from "../../../infrastructure/Api/BrowserTabsApi";
import TabFactory from "../../../infrastructure/Factory/TabFactory";
import AppLoader from "../../../elements/Loader/AppLoader";
import {AppContext} from "../../App/AppContext";
import {
    filterByEnableStatus,
    sliceString,
    uniqueTabsToSave,
    windowIncludesTabByUrl
} from "../../../infrastructure/helpers";
import apiClient from "../../../infrastructure/Api/ApiClient";
import {MdCancel, MdCheck, MdEdit} from "react-icons/md";
import {infoToast, successToast} from "../../../infrastructure/toast";
import {Dropdown, DropdownButton} from "react-bootstrap";
import Collapse from "react-bootstrap/Collapse";

class SessionWindowItem extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            tabsCollapsed: false,
            onEdit: false,
            editForm: {
                value: props.window.name ? props.window.name : ''
            }
        };
    }

    addFocusedTab = () => {
        new BrowserWindowsApi().findFocused((focusedWindow) => {
            new BrowserTabsApi().findByWindow(focusedWindow.id, (tabs) => {
                const activeTab = tabs.find(t => t.active === true);
                const tab = new TabFactory(activeTab).json();

                if (windowIncludesTabByUrl(this.props.window, tab.url)) {
                    infoToast(this.context.t('already-saved'));
                    return;
                }

                this.setState({isLoading: true});
                apiClient(this.context.token, this.context.deviceId).post('/window-tabs', {window: this.props.window.id, tabs: [tab]}).then(tabsResponse => {
                    let sessions = this.props.getSessions();

                    for (let i = 0; i < sessions.length; i++) {
                        if (sessions[i].id === this.props.session.id) {
                            for (let j = 0; j < sessions[i].windows.length; j++) {
                                if (sessions[i].windows[j].id === this.props.window.id) {
                                    tabsResponse.data.forEach(savedTab => {
                                        sessions[i].windows[j].tabs.push(savedTab);
                                    })

                                    break;
                                }
                            }

                            break;
                        }
                    }

                    this.props.updateSessions(sessions);
                    this.setState({isLoading: false});

                    document
                        .getElementById(`window-tab-${tabsResponse.data[0].id}`)
                        .scrollIntoView({behavior: 'smooth'});
                })
            })
        })
    }

    openTabs = () => {
        const [enabledTabs] = filterByEnableStatus(this.props.window.tabs);
        const browserTabsApi = new BrowserTabsApi();

        enabledTabs.forEach(tab => {
            browserTabsApi.createTab(tab.url);
        })
    }

    replaceTabs = () => {
        this.setState({isLoading: true});

        new BrowserWindowsApi().findFocused((focusedWindow) => {
            new BrowserTabsApi().findByWindow(focusedWindow.id, (tabs) => {
                const normalizedTabs = TabFactory.createMany(tabs);

                apiClient(this.context.token, this.context.deviceId).patch(`/session-windows/${this.props.window.id}/replace-tabs`, {tabs: normalizedTabs}).then(tabsResponse => {
                    let sessions = this.props.getSessions();

                    for (let i = 0; i < sessions.length; i++) {
                        if (sessions[i].id === this.props.session.id) {
                            for (let j = 0; j < sessions[i].windows.length; j++) {
                                if (sessions[i].windows[j].id === this.props.window.id) {
                                    sessions[i].windows[j].tabs = tabsResponse.data;

                                    break;
                                }
                            }

                            break;
                        }
                    }

                    this.props.updateSessions(sessions);
                    this.setState({isLoading: false});

                    document
                        .getElementById(`window-tab-${tabsResponse.data[0].id}`)
                        .scrollIntoView({behavior: 'smooth'});
                })
            })
        })
    }

    mergeTabs = () => {
        new BrowserWindowsApi().findFocused((focusedWindow) => {
            new BrowserTabsApi().findByWindow(focusedWindow.id, (tabs) => {
                const normalizedTabs = TabFactory.createMany(tabs);
                const uniqueTabs = uniqueTabsToSave(this.props.window, normalizedTabs);

                if (uniqueTabs.length === 0) {
                    infoToast(this.context.t('already-saved'));
                    return;
                }

                this.setState({isLoading: true});
                apiClient(this.context.token, this.context.deviceId).post('/window-tabs', {window: this.props.window.id, tabs: uniqueTabs}).then(tabsResponse => {
                    let sessions = this.props.getSessions();

                    for (let i = 0; i < sessions.length; i++) {
                        if (sessions[i].id === this.props.session.id) {
                            for (let j = 0; j < sessions[i].windows.length; j++) {
                                if (sessions[i].windows[j].id === this.props.window.id) {
                                    tabsResponse.data.forEach(savedTab => {
                                        sessions[i].windows[j].tabs.push(savedTab);
                                    })

                                    break;
                                }
                            }

                            break;
                        }
                    }

                    this.props.updateSessions(sessions);
                    this.setState({isLoading: false});

                    document
                        .getElementById(`window-tab-${tabsResponse.data[0].id}`)
                        .scrollIntoView({behavior: 'smooth'});
                })
            })
        })
    }

    openWindow = () => {
        new BrowserWindowsApi().openWindows([this.props.window]);
    }

    deleteWindow = () => {
        this.setState({isLoading: true});

        apiClient(this.context.token, this.context.deviceId).delete(`/session-windows/${this.props.window.id}`)
            .then(() => {
                const sessions = this.props.getSessions();

                for (let i = 0; i < sessions.length; i++) {
                    if (sessions[i].id === this.props.session.id) {
                        for (let j = 0; j < sessions[i].windows.length; j++) {
                            if (sessions[i].windows[j].id === this.props.window.id) {
                                sessions[i].windows.splice(j, 1);

                                break;
                            }
                        }
                    }
                }

                this.props.updateSessions(sessions);
                this.setState({isLoading: false});

                successToast(this.context.t('successfully-deleted'));
            })
    }

    toggleTabs = () => {
        this.setState({tabsCollapsed: !this.state.tabsCollapsed});
    }

    toggleEdit = () => {
        const form = this.state.editForm;
        form.touched = false;
        form.valid = true;
        form.value = this.props.window.name;

        this.setState({onEdit: !this.state.onEdit, editForm: form});
    }

    handleEdit = (e) => {
        const form = this.state.editForm;
        form.value = e.target.value;

        this.setState({editForm: form});
    }

    editWindow = () => {
        this.setState({isLoading: true});

        const newName = this.state.editForm.value === '' ? null : this.state.editForm.value;
        apiClient(this.context.token, this.context.deviceId).patch(`/session-windows/${this.props.window.id}`, {name: newName})
            .then(res => {
                const sessions = this.props.getSessions();

                for (let i = 0; i < sessions.length; i++) {
                    if (sessions[i].id === this.props.session.id) {
                        for (let j = 0; j < sessions[i].windows.length; j++) {
                            if (sessions[i].windows[j].id === this.props.window.id) {
                                res.data.tabs.sort((a, b) => (a['order_index'] > b['order_index']) ? 1 : -1);
                                sessions[i].windows[j] = res.data;

                                break;
                            }
                        }
                        break;
                    }
                }

                this.props.updateSessions(sessions);
                this.setState({isLoading: false, onEdit: false});

                successToast(this.context.t('successfully-saved'));
            })
    }

    render() {
        const t = this.context.t;
        const isEnabled = this.props.window['is_enabled'];
        const [enabledTabs, disabledTabs] = filterByEnableStatus(this.props.window.tabs);
        const hasDisabledTabs = disabledTabs.length > 0;

        return (
            this.state.isLoading ? <AppLoader/> :
                <>
                    <div className={'container session-window shadow'}>
                        <div className={'container px-0'} id={`session-window-${this.props.window.id}`}>
                            <div className={'card'}>
                                <div className={'card-header'}>
                                    {this.state.onEdit ?
                                    <div className={'container mt-10 mb-10 text-center edit-form'}>
                                        <div className={'row justify-content-center'}>
                                            <div className={'col-9'}>
                                                <input className={`form-control fw-500`} value={this.state.editForm.value} onChange={this.handleEdit}/>
                                            </div>
                                            <div className={'col-3'}>
                                                <div>
                                                    <Button size={'sm'} onClick={this.editWindow} variant={'transparent'}><MdCheck/></Button>
                                                    <Button className={'text-danger'} size={'sm'} onClick={this.toggleEdit} variant={'transparent'}><MdCancel/></Button>
                                                </div>
                                            </div>
                                        </div>
                                    </div> :
                                    <div>
                                        <h6 className="header-title">
                                        <span className={'session-window-title'} onClick={this.toggleTabs}>
                                            {this.props.window.name === null ? `${this.context.t('Window')} #${this.props.index + 1}` : sliceString(this.props.window.name, 30)}
                                            {` (${this.context.t('tabs')}: ${this.props.window.tabs.length})`}
                                        </span>
                                            <span className={'buttons'}>
                                            <ButtonGroup>
                                                {isEnabled && <Button data-bs-toggle="tooltip" data-bs-placement="top" title={t('Open')} onClick={this.openWindow} className={'ml-1'} variant={'transparent'}><FaExternalLinkAlt/></Button>}
                                                {isEnabled && <Button data-bs-toggle="tooltip" data-bs-placement="top" title={t('Open here')} onClick={this.openTabs} className={'ml-1'} variant={'transparent'}><FaExternalLinkSquareAlt/></Button>}
                                                {isEnabled && <Button data-bs-toggle="tooltip" data-bs-placement="top" title={t('Merge tabs')} onClick={this.mergeTabs} className={'ml-1'} variant={'transparent'}><FaObjectGroup/></Button>}
                                                {isEnabled && <Button data-bs-toggle="tooltip" data-bs-placement="top" title={t('Replace tabs')} onClick={this.replaceTabs} className={'ml-1'} variant={'transparent'}><FaExchangeAlt/></Button>}
                                                {isEnabled && <Button data-bs-toggle="tooltip" data-bs-placement="top" title={t('add-focused-tab')} onClick={this.addFocusedTab} className={'ml-1'} variant={'transparent'}><FaPlus/></Button>}
                                                {isEnabled && <Button onClick={this.toggleEdit} className={'ml-1'} variant={'transparent'}><MdEdit/></Button>}
                                                <DropdownButton className={'no-dropdown-border'} variant={'transparent'} as={ButtonGroup} title={<FaEllipsisV />}>
                                                        <Dropdown.Item disabled={!isEnabled} onClick={isEnabled ? this.openWindow : () => {}}>
                                                            <FaExternalLinkAlt/>
                                                            <span className={'ml-05 fw-500'}>{t('Open')}</span>
                                                        </Dropdown.Item>
                                                        <Dropdown.Item disabled={!isEnabled} onClick={isEnabled ? this.openTabs : () => {}}>
                                                            <FaExternalLinkSquareAlt/>
                                                            <span className={'ml-05 fw-500'}>{t('Open here')}</span>
                                                        </Dropdown.Item>
                                                        <Dropdown.Item disabled={!isEnabled} onClick={isEnabled ? this.mergeTabs : () => {}}>
                                                            <FaObjectGroup/>
                                                            <span className={'ml-05 fw-500'}>{t('Merge tabs')}</span>
                                                        </Dropdown.Item>
                                                        <Dropdown.Item disabled={!isEnabled} onClick={isEnabled ? this.replaceTabs : () => {}}>
                                                            <FaExchangeAlt/>
                                                            <span className={'ml-05 fw-500'}>{t('Replace tabs')}</span>
                                                        </Dropdown.Item>
                                                        <Dropdown.Item disabled={!isEnabled} onClick={isEnabled ? this.addFocusedTab : () => {}}>
                                                            <FaPlus/>
                                                            <span className={'ml-05 fw-500'}>{t('add-focused-tab')}</span>
                                                        </Dropdown.Item>
                                                        <Dropdown.Divider />
                                                        <Dropdown.Item className={'text-danger'} onClick={this.deleteWindow}>
                                                            <FaTrash/>
                                                            <span className={'ml-05'}>{this.context.t('delete')}</span>
                                                        </Dropdown.Item>
                                                    </DropdownButton>
                                            </ButtonGroup>
                                        </span>
                                        </h6>
                                        {!isEnabled && <div className="text-danger mb-10">{t('disabled-window')}</div>}
                                        {isEnabled && hasDisabledTabs && <div className="text-danger mb-10">{t('disabled-tabs')}</div>}
                                    </div>}
                                </div>
                            </div>
                        </div>
                        <Collapse in={this.state.tabsCollapsed}>
                            <div>
                                {this.props.window.tabs.length === 0 ?
                                    <h6 className={'text-info text-center mt-10 mb-10 pb-10'}>
                                        {this.context.t('none-saved-tabs')}
                                    </h6> : <ul className="list-group">
                                        {this.props.window.tabs.map(tab => (<WindowTabItem
                                            session={this.props.session}
                                            window={this.props.window}
                                            tab={tab}
                                            getSessions={this.props.getSessions}
                                            updateSessions={this.props.updateSessions}
                                            restrictions={this.props.restrictions}
                                        />))}
                                    </ul>
                                }
                            </div>
                        </Collapse>
                    </div>
                </>
        )
    }
}

export default SessionWindowItem;