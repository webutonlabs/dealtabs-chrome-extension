import React from "react";
import apiClient from "../../infrastructure/Api/ApiClient";
import {AppContext} from "../App/AppContext";
import {NoneArchivedTabs} from "./NoneArchivedTabs";
import {ArchivedTabList} from "./ArchivedTabList";
import AppLoader from "../../elements/Loader/AppLoader";
import LoadMore from "../../infrastructure/Elements/LoadMore";

export class ArchivedTabsComponent extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            tabs: [],
            page: 1,
            perPage: 20,
            nextPage: null,
            totalTabs: 0
        }
    }

    componentDidMount() {
        this.fetchTabs();
    }

    fetchTabs = () => {
        this.setState({isLoading: true});

        apiClient(this.context.token, this.context.deviceId).get(`/archived-tabs?&per_page=${this.state.perPage}&page=${this.state.page}`)
            .then(res => {
                let tabs = [...this.state.tabs, ...res.data];

                this.setState({
                    isLoading: false,
                    page: this.state.page + 1,
                    nextPage: res.headers['x-pagination-next-page'] === 'null' ? null : Number(res.headers['x-pagination-next-page']),
                    tabs: tabs,
                    totalTabs: Number(res.headers['x-pagination-total'])
                });

                if (this.state.page > 2) {
                    window.scrollTo(0, document.body.scrollHeight);
                }
            })
    }

    setTabs = (tabs) => {
        this.setState({tabs: tabs});
    }

    getTabs = () => {
        return this.state.tabs;
    }

    loadMore = () => {
        if (null !== this.state.nextPage) {
            this.fetchTabs();
        }
    }

    render() {
        return (
            this.state.isLoading ? <AppLoader/> :
            <>
                <div className={'container mt-2'}>
                    <div className={'row'}>
                        <div className={'col-12'}>
                            <div className={'alert alert-warning'}>
                                {this.context.t('archived-tabs-warning-1')}
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.tabs.length === 0 && <NoneArchivedTabs />}
                {this.state.tabs.length > 0 &&
                    <ArchivedTabList
                        tabs={this.state.tabs}
                        getTabs={this.getTabs}
                        setTabs={this.setTabs}
                    />
                }
                <div className={'container'}>
                    {null !== this.state.nextPage && <LoadMore loadMore={this.loadMore}/>}
                </div>
            </>
        )
    }
}