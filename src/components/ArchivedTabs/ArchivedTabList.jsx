import React from "react";
import {ArchivedTab} from "./ArchivedTab";

export class ArchivedTabList extends React.Component {
    render() {
        return (
            <>
                <div className={'container'}>
                    {
                        this.props.tabs.map(tab => {
                            return <ArchivedTab
                                tab={tab}
                                key={tab.id}
                                getTabs={this.props.getTabs}
                                setTabs={this.props.setTabs}
                            />
                        })
                    }
                </div>
            </>
        )
    }
}