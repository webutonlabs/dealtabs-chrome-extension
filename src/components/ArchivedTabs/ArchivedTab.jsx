import React from "react";
import BrowserTabsApi from "../../infrastructure/Api/BrowserTabsApi";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Button from "react-bootstrap/Button";
import {FaTrash} from "react-icons/fa";
import apiClient from "../../infrastructure/Api/ApiClient";
import {AppContext} from "../App/AppContext";
import {sliceString} from "../../infrastructure/helpers";
import AppLoader from "../../elements/Loader/AppLoader";
import TabIcon from "../../elements/TabIcon";

export class ArchivedTab extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false
        }
    }

    openTab = () => {
        new BrowserTabsApi().createTab(this.props.tab.url, false);
    }

    deleteTab = () => {
        this.setState({isLoading: true})

        apiClient(this.context.token, this.context.deviceId).delete(`/archived-tabs/${this.props.tab.id}`).then(() => {
            let tabs = this.props.getTabs();

            for (let i = 0; i < tabs.length; i++) {
                if (tabs[i].id === this.props.tab.id) {
                    tabs.splice(i, 1);
                    break;
                }
            }

            this.setState({isLoading: false})
            this.props.setTabs(tabs);
        })
    }

    render() {
        const img = this.props.tab.icon === null ? `${process.env.REACT_APP_SITE_HOST}/no-tab-icon-replacer.png` : this.props.tab.icon

        return (
            <>
                <div className="archived-tab-card card col-12 px-0">
                    <div className="according">
                        <div className="card shadow-sm">
                            <div className="card-header archived-tab">
                                {this.state.isLoading ? <AppLoader/> :
                                    <>
                                        <div className={`title text-left`}>
                                            <a className={`card-link`} onClick={this.openTab}>
                                                <TabIcon src={this.props.tab.icon}/>
                                                <span className={'ml-05'}>{sliceString(this.props.tab.name, 60)}</span>
                                            </a>
                                        </div>
                                        <div className={`action text-center`}>
                                            <ButtonGroup className={'ml-15'}>
                                                <Button onClick={this.deleteTab} size={'sm'} variant={'transparent'} className={'text-danger delete-btn'}>
                                                    <FaTrash/>
                                                </Button>
                                            </ButtonGroup>
                                        </div>
                                    </>
                                }
                            </div>
                            <div className={'card-body p-0'}>
                                <button className={'btn btn-link btn-sm text-left'} onClick={this.openTab}>
                                    {sliceString(this.props.tab.url, 80)}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}