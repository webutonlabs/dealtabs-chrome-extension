import React from "react";
import {AppContext} from "../App/AppContext";

export class NoneArchivedTabs extends React.Component {
    static contextType = AppContext;

    render() {
        return (
            <>
                <div className={'container'}>
                    <div className={'row'}>
                        <div className={'col-12'}>
                            <div className={'alert alert-success'}>{this.context.t('none-archived-tabs')}</div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}