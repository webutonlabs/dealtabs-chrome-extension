import React, {Component, createContext} from 'react'
import BrowserCookiesApi from "../../infrastructure/Api/BrowserCookiesApi";
import apiClient from "../../infrastructure/Api/ApiClient";
import {isUserPremium} from "../../infrastructure/helpers";

export const AppContext = createContext(undefined)

class AppContextProvider extends Component {
    state = {
        isLoading: true,
        isAuthenticated: false,
        deviceId: null,
        token: null,
        user: {
            id: '',
            name: '',
            surname: '',
            email: '',
            isEmailConfirmed: false,
            locale: 'en',
            membership: {
                membershipType: false,
                subscriptionType: false,
                subscriptionExpires: false,
                isAllowedTrial: null,
                isCancelled: false
            },
        },
        translator: this.props.translator,
        t: this.props.t
    }

    componentDidMount() {
        const browserCookies = new BrowserCookiesApi();

        browserCookies.iterateAll(process.env.REACT_APP_SITE_DOMAIN, (cookies) => {
            browserCookies.setCookies(cookies);

            const tokenCookie = browserCookies.findCookieOrNull('token');
            const deviceIdCookie = browserCookies.findCookieOrNull('device-id');
            const liveSyncCookie = browserCookies.findCookieOrNull('live-sync');

            if (null !== tokenCookie && null !== deviceIdCookie) {
                apiClient(tokenCookie.value, deviceIdCookie.value)
                    .get('/user')
                    .then((user) => {
                        this.setState({
                            isAuthenticated: true,
                            isLoading: false,
                            deviceId: deviceIdCookie.value,
                            token: tokenCookie.value,
                            user: {
                                id: user.data.id,
                                name: user.data.name,
                                surname: user.data.surname,
                                email: user.data.email,
                                isEmailConfirmed: user.data['is_email_confirmed'],
                                locale: user.data['locale'],
                                membership: {
                                    membershipType: user.data.membership['membership_type'],
                                    subscriptionType: user.data.membership['subscription_type'],
                                    subscriptionExpires: user.data.membership['subscription_expires'],
                                    isAllowedTrial: user.data.membership['is_allowed_trial'],
                                    isCancelled: user.data.membership['is_cancelled']
                                },
                            }
                        });

                        this.state.translator.changeLanguage(user.data.locale);

                        if (null !== liveSyncCookie) {
                            if (!isUserPremium(this.state.user)) {
                                const date = new Date();
                                date.setFullYear(2050);

                                new BrowserCookiesApi().createCookie(
                                    () => {},
                                    'live-sync',
                                    'false',
                                    date.getTime()
                                );
                            }
                        }
                    })
                    .catch(() => {
                        setTimeout(() => {
                            this.setState({isLoading: false})
                        }, 1000)
                    })
            } else {
                setTimeout(() => this.setState({isLoading: false}), 1000)
            }
        })
    }

    render() {
        return (
            <AppContext.Provider value={{...this.state}}>
                {this.props.children}
            </AppContext.Provider>
        )
    }
}

export default AppContextProvider
