import React from "react";
import SessionsComponent from "../Sessions/SessionsComponent";
import LiveSynchronizationComponent from "../LiveSynchronization/LiveSynchronizationComponent";
import 'react-toastify/dist/ReactToastify.css';
import {ArchivedTabsComponent} from "../ArchivedTabs/ArchivedTabsComponent";

class ResolveComponent extends React.Component {
    render() {
        return (
            <>
                {this.props.route === '/sessions' && <SessionsComponent />}
                {this.props.route === '/live-synchronization' && <LiveSynchronizationComponent />}
                {this.props.route === '/archived-tabs' && <ArchivedTabsComponent />}
            </>
        )
    }
}

export default ResolveComponent;