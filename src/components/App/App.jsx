import React from 'react';
import {BrowserRouter as Router} from 'react-router-dom'
import AppRoutes from "./AppRoutes";

import "../../assets/styles/default-css.css";
import "../../assets/styles/responsive.css";
import "../../assets/styles/styles.css";
import "../../assets/styles/typography.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import "../../assets/styles/custom.css";

import {useTranslation} from "react-i18next";
import AppContextProvider from "./AppContext";
import {ToastContainer} from "react-toastify";

function App() {
    const {t, i18n} = useTranslation();

    return (
        <AppContextProvider translator={i18n} t={t}>
            <Router>
                <AppRoutes/>
            </Router>
        </AppContextProvider>
    )
}

export default App;
