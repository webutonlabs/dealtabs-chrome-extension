import React, {useContext, useState} from 'react'
import {AppContext} from './AppContext'

import {Redirect, Route, Switch} from "react-router-dom";
import Layout from "../../layouts/Layout";
import AppLoader from "../../elements/Loader/AppLoader";
import LoginComponent from "../User/LoginComponent";
import ResolveComponent from "./ResolveComponent";

const routes = [
    {path: '/app', component: ResolveComponent},
    {path: '/login', component: LoginComponent}
]

const AppRoutes = () => {
    const {isAuthenticated, isLoading} = useContext(AppContext);
    const [currentRoute, setRoute] = useState('/sessions');

    /**
     * Router does not work as expected in browser extension eco system
     * So we have one route with ResolveComponent component
     * which loads needed component depending on route
     */
    return isLoading ? (
        <AppLoader/>
    ) : isAuthenticated ? (
        <Layout setRoute={setRoute}>
            <Switch>
                {routes.map((route, index) => (
                    <Route key={index} path={route.path} exact={route.exact}>
                        <route.component route={currentRoute}/>
                    </Route>
                ))}
                <Redirect to='/app'/>
            </Switch>
        </Layout>
    ) : <LoginComponent/>
}

export default AppRoutes