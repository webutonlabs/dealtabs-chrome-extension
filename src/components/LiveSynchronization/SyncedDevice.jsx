import React from "react";
import BrowserTabsApi from "../../infrastructure/Api/BrowserTabsApi";
import {sliceString} from "../../infrastructure/helpers";
import {FaTrash} from "react-icons/fa";
import apiClient from "../../infrastructure/Api/ApiClient";
import {AppContext} from "../App/AppContext";
import {successToast} from "../../infrastructure/toast";
import TabIcon from "../../elements/TabIcon";

class SyncedDevice extends React.Component {
    static contextType = AppContext;

    openTab = (url) => {
        new BrowserTabsApi().createTab(url, false);
    }

    deleteDevice = () => {
        apiClient(this.context.token, this.context.deviceId).delete('/live-sync/delete-device').then(res => {
            successToast(this.context.t('successfully-deleted'));
        })
    }

    render() {
        return (
            <>
                <div className={'card shadow mt-2'}>
                    <div className={'card-header text-center synced-device-tabs-header'}>
                        <div className={'synced-device-title'}>
                            <div style={{width: this.context.deviceId === this.props.device.id ? '100%' : '85%'}}>
                                <span className={'name'} onClick={this.toggleEdit}>
                                    {this.props.device.name}
                                </span>
                            </div>
                            {this.context.deviceId !== this.props.device.id &&
                                <div>
                                    <button onClick={this.deleteDevice} className={'btn btn-danger btn-sm'}>
                                        <span><FaTrash/></span>
                                    </button>
                                </div>
                            }
                        </div>
                    </div>
                    <div className={'card-body p-0'}>
                        <ul className="list-group synced-device-tabs">
                            {this.props.device.tabs.map(syncedTab => {
                                return <li key={syncedTab.id} className={`list-group-item ${syncedTab['is_active'] ? 'active' : ''}`}>
                                    <span>
                                        <TabIcon src={syncedTab.icon}/>
                                    </span>
                                    <span className={'ml-2'}>
                                        <a href="#" onClick={() => {this.openTab(syncedTab.url);}}>
                                            {sliceString(syncedTab.name, 80)}
                                        </a>
                                    </span>
                                </li>
                            })}
                        </ul>
                    </div>
                </div>
            </>
        )
    }
}

export default SyncedDevice;