import React from "react";
import SyncedDevice from "./SyncedDevice";

class ListSyncedDevices extends React.Component {
    render() {
        return (
            <>
                <div className={'container'}>
                    <div className={'row'}>
                        <div className={'col-12'}>
                            {this.props.syncedDevices.map(syncedDevice => {
                                return <SyncedDevice
                                    key={syncedDevice.id}
                                    device={syncedDevice}
                                />
                            })}
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default ListSyncedDevices;