import React from "react";
import SyncedDeviceComponent from "./SyncedDevice/SyncedDeviceComponent";
import apiClient from "../../infrastructure/Api/ApiClient";
import {AppContext} from "../App/AppContext";
import BrowserCookiesApi from "../../infrastructure/Api/BrowserCookiesApi";
import ListSyncedDevices from "./ListSyncedDevices";

class LiveSynchronizationComponent extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);

        this.state = {
            device: {
                id: null,
                name: '',
                isSyncEnabled: false
            },
            syncedDevices: [],
            updateIntervalId: null
        }
    }

    componentDidMount() {
        apiClient(this.context.token, this.context.deviceId).get('/live-sync/view-device').then(res => {
            const device = {
                id: res.data.id,
                name: res.data.name,
                isSyncEnabled: res.data['is_sync_enabled']
            };

            this.setState({device: device});
            this.updateSyncEnabledStatus(device);
        });

        const updateIntervalId = setInterval(() => {
            apiClient(this.context.token, this.context.deviceId).get('/live-sync').then(res => {
                const devices = res.data;

                // moving current device to the start
                // devices.sort((x, y) => {
                //     return x.id === this.context.deviceId ? -1 : y === this.context.deviceId ? 1 : 0;
                // });

                // moving current device to the end
                for (let i = 0; i < devices.length; i++) {
                    if (devices[i].id === this.context.deviceId) {
                        devices.push(devices.splice(i, 1)[0]);
                        break;
                    }
                }

                for (const device of devices) {
                    device.tabs.sort((a, b) => {return a.orderIndex - b.orderIndex});
                }

                this.setState({syncedDevices: devices});
            });
        }, 1000);

        this.setState({updateIntervalId: updateIntervalId});
    }

    componentWillUnmount() {
        clearInterval(this.state.updateIntervalId);
    }

    setDevice = (device) => {
        this.setState({device: device});
    }

    updateSyncEnabledStatus = (device) => {
        const cookieCreatedCallback = (cookie) => {
            apiClient(this.context.token, this.context.deviceId).patch('/live-sync/edit-device', {is_sync_enabled: device.isSyncEnabled}).then(res => {
                this.setDevice(device);
            })
        }

        const date = new Date();
        date.setFullYear(2050);

        new BrowserCookiesApi().createCookie(
            cookieCreatedCallback,
            'live-sync',
            device.isSyncEnabled ? 'true' : 'false',
            date.getTime()
        );
    }

    render() {
        return (
            <>
                <SyncedDeviceComponent
                    device={this.state.device}
                    setDevice={this.setDevice}
                    updateSyncEnabledStatus={this.updateSyncEnabledStatus}
                />
                <ListSyncedDevices syncedDevices={this.state.syncedDevices}/>
            </>
        )
    }
}

export default LiveSynchronizationComponent;