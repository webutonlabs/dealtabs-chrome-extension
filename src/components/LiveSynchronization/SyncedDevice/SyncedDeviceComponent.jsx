import React from "react";
import apiClient from "../../../infrastructure/Api/ApiClient";
import {AppContext} from "../../App/AppContext";
import Button from "react-bootstrap/Button";
import {MdCancel, MdCheck} from "react-icons/md";
import {successToast} from "../../../infrastructure/toast";
import {FaToggleOff, FaToggleOn} from "react-icons/all";

class SyncedDeviceComponent extends React.Component {
    static contextType = AppContext;

    constructor(props) {
        super(props);
        this.state = {
            formName: {
                name: '',
                valid: true,
                touched: false,
                onEdit: false
            }
        }
    }

    toggleEdit = () => {
        const formName = this.state.formName;
        formName.name = this.props.device.name;
        formName.touched = false;
        formName.valid = true;
        formName.onEdit = !formName.onEdit;

        this.setState({formName: formName});
    }

    onChangeName = (e) => {
        const name = e.currentTarget.value;
        const formName = this.state.formName;

        formName.name = name;
        formName.touched = true;
        formName.valid = name !== '';

        this.setState({formName: formName});
    }

    changeName = () => {
        const newName = this.state.formName.name;

        apiClient(this.context.token, this.context.deviceId).patch('/live-sync/edit-device', {name: newName}).then(res => {
            const device = this.props.device;
            device.name = newName;

            const formName = this.state.formName;
            formName.name = newName;
            formName.touched = false;
            formName.valid = true;
            formName.onEdit = false;

            this.props.setDevice(device);
            this.setState({formName: formName});
            successToast(this.context.t('successfully-saved'))
        });
    }


    toggleSync = () => {
        const device = this.props.device;
        device.isSyncEnabled = !device.isSyncEnabled;

        this.props.updateSyncEnabledStatus(device);
        successToast(this.context.t('successfully-saved'));
    }

    render() {
        return (
            <div className={'container mt-2'}>
                <div className={'row'}>
                    <div className={'col-12'}>
                        <div className={'card shadow'}>
                            <div className={'card-header text-center sync-device-header'}>
                                {this.state.formName.onEdit ?
                                    <>
                                        <div className={'row'}>
                                            <div className={'col-9'}>
                                                <input
                                                    onChange={this.onChangeName}
                                                    value={this.state.formName.name}
                                                    className={`form-control ${this.state.formName.touched ? this.state.formName.valid ? 'is-valid' : 'is-invalid' : ''}`}
                                                    type={'text'}
                                                />
                                            </div>
                                            <div className={'col-3'}>
                                                <Button onClick={this.changeName} disabled={!this.state.formName.valid} size={'sm'} variant={'transparent'}><MdCheck/></Button>
                                                <Button className={'text-danger'} size={'sm'} onClick={this.toggleEdit} variant={'transparent'}><MdCancel/></Button>
                                            </div>
                                        </div>
                                    </> :
                                    <>
                                        <div className={'synced-device-title'}>
                                            <div style={{width: '75%'}}>
                                                <span className={'name'} onClick={this.toggleEdit}>
                                                    {this.props.device.name}
                                                </span>
                                            </div>
                                            <div>
                                                {this.props.device.isSyncEnabled &&
                                                <button onClick={this.toggleSync} className={'btn btn-success btn-sm'}>
                                                    <span>{this.context.t('sync-enabled')}</span>
                                                    <span className={'ml-2'}><FaToggleOn /></span>
                                                </button>
                                                }
                                                {!this.props.device.isSyncEnabled &&
                                                <button onClick={this.toggleSync} className={'btn btn-danger btn-sm'}>
                                                    <span>{this.context.t('sync-disabled')}</span>
                                                    <span className={'ml-2'}><FaToggleOff /></span>
                                                </button>
                                                }
                                            </div>
                                        </div>
                                    </>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SyncedDeviceComponent;