import React from "react";
import {AppContext} from "../App/AppContext";
import BrowserTabsApi from "../../infrastructure/Api/BrowserTabsApi";

class LoginComponent extends React.Component {
    static contextType = AppContext;

    render() {
        const t = this.context.t;
        const browserTabsApi = new BrowserTabsApi();

        return (
            <div className="error-area ptb--50 text-center">
                <div className="container">
                    <div className="error-content shadow-lg">
                        <h6>{t('Authentication needed')}</h6>
                        <p>{t('login-to-start')}</p>
                        <button className={'btn'} onClick={() => {browserTabsApi.createTab(process.env.REACT_APP_SITE_HOST + '/user')}}>{t('sign-in-page')}</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default LoginComponent;